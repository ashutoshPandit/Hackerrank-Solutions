process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function main() {
    var t = parseInt(readLine());
    
    var countStart = 3;
    var countDown = 0;
    var slotInit = 1;
    var slotEnd = 3;
  
    while (!(t >= slotInit && t <= slotEnd)) {
        slotInit = slotEnd + 1;
        slotEnd = slotEnd + (countStart * 2);
        countStart = countStart * 2;
    }
    
    countDown = t - slotInit;
    console.log(countStart - countDown);
}
