process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function main() {
    h = readLine().split(' ');
     var heights = h.map(Number);
    var word = readLine();
    let maxHeight = 0;
    for(let i = 0; i < word.length; i++) {
        const charIndex = word[i].codePointAt() - 97;
        const height = heights[charIndex];
        if(height > maxHeight) {
            maxHeight = height;
        }
    }
 
    const square = word.length * maxHeight;
    console.log(square);
}
