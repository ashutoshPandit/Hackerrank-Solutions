process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function main() {
    var t = parseInt(readLine());
    for(var a0 = 0; a0 < t; a0++){
        var n = parseInt(readLine());
        var height = 1;
        if(n === 0){
            console.log(height);
        }
        else if(n === 1) {
            console.log(height + 1);
        }
        else {
            var springSeason = true;
            while(n >= 1) {
                if(springSeason) {
                    height *= 2;  
                    n--;
                    springSeason = false;
                }
                else {
                    height += 1;
                    n--;
                    springSeason = true;
                }
            }
            console.log(height);
        }
    }
}

