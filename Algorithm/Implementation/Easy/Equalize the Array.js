function processData(input) {
    //Enter your code here
    var lines = input.split('\n');
    var array = lines[1].split(' ');
    var numCounts = {};
    for(var i = 0; i < array.length; i++) {
        var count = numCounts[array[i]] || 0;
        numCounts[array[i]] = count + 1;
    }
    var maxCount = 0;
    for(var p in numCounts) {
        if(numCounts.hasOwnProperty(p) && numCounts[p] > maxCount) {
            maxCount = numCounts[p];
        }
    }
    console.log(array.length - maxCount);
} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
