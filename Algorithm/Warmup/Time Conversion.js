process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function main() {
    var time = readLine();
     var isAfterNoon = ( time.toUpperCase().indexOf('P') > -1 ) ? true : false;
    
    time = time.split(':');
    
    if( isAfterNoon && time[0] != '12' ) {
        time[0] = (parseInt(time[0]) + 12).toString();
    } else if( !isAfterNoon && time[0] === '12' ) {
        time[0] = '00';
    }
    
    time[2] = ( time[2].indexOf('A') > -1 ) ? time[2].split('A')[0] : time[2].split('P')[0];
    
    time = time[0] + ':' + time[1] + ':' + time[2]; 
    console.log(time);

}
