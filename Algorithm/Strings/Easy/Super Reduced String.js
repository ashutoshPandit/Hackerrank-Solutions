function processData(input) {
    var line = input.split('\n')[0];   
    var acc = "";
    for(var i = 0; i < line.length; i++) {
        var c = line[i];
        if(acc.length > 0 && acc[acc.length - 1] === c) {
            acc = acc.slice(0, acc.length - 1);
            //console.log("slice"+i+acc);
        } else {
            acc += c;
            //console.log("not"+i+acc);
        }
    }
    console.log(acc.length === 0 ? "Empty String" : acc);
    
} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
