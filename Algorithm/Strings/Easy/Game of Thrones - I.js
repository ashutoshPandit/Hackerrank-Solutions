function isOdd(number) {
  return number % 2 === 1;
}

function isAnagram(input) {
  var letters = [].reduce.call(input, function(l, c) {
      !l[c] && (l[c] = 0);
      l[c]++;
      return l;
  }, {});

  var canBeOddNumber = isOdd(input.length),
      wasOddNumber = false;
  for (var letter in letters) {
    if (isOdd(letters[letter])) {
      if (!canBeOddNumber || wasOddNumber) {
        return false;
      }
      wasOddNumber = true;
    }
  }
  return true;
}

function processData(input) {
  process.stdout.write(isAnagram(input) ? 'YES' : 'NO');
} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
