//Method 1

function processData(input) {
    var lines = input.split('\n');
    //var n = input.split('\n')[0].split(" ")[0];//.map(Number).join();
	var n = parseInt(lines.shift(), 10);

	while (lines.length > 0) {
		var str = lines.shift();
		
 var deletions = 0 ;
		for (var i = 1; i < str.length; i++) {
			if (str[i] === str[i - 1]) {
               
				deletions++;
			}
		}

		console.log(deletions);
	};
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

//Method 2

function getCountDeleting(word) {
  var count = 0,
      previousSymbol = word[0];

  for (var i = 1, l = word.length; i < l; i++) {
    if (word[i] === previousSymbol) {
      count++;
    } else {
      previousSymbol = word[i];
    }
  }

  return count;
}

function processData(input) {
  var data = input.split('\n').slice(1);
  data = data.map(getCountDeleting);
  process.stdout.write(data.join('\n'));
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

//Method 3

function processData(input) {
   var lines = input.split("\n");
    console.log(lines.slice(1, lines.length).map(deletions).join("\n"));
}
 
function deletions(s) {
    var last = s.charAt(0);
    var dels = 0;
    for(var i = 1; i < s.length; i++) {
        if(last === s.charAt(i)) {
            dels++;
        } else {
            last = s.charAt(i);
        }
    }
    return dels;
}
 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
